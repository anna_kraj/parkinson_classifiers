import os

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

delimiter = ','
filename = os.path.join(__location__, 'pd_and_c_data.csv')


def get_data_from_table():
    data_matrix = {}
    parkinson_patients = {}
    with open(filename) as data:
        lines = data.readlines()
        features = [feature.strip().upper().replace(' ', '_') for feature in lines[0].split(delimiter)]
        for i in range(1, len(lines)):
            line = lines[i].split(delimiter)
            code = line[0].strip()
            row = {}
            if len(code) > 0:
                for j in range(1, len(line)):
                    value = line[j].strip()
                    try:
                        value = float(value)
                        row[features[j]] = value
                    except ValueError:
                        value = -1
                data_matrix[code] = row
                if code.startswith('SG'):
                    parkinson_patients[code] = row
    return data_matrix, parkinson_patients


if __name__ == '__main__':
    matrix, pd_patients = get_data_from_table()
