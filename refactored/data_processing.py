import os

import numpy as np
import pandas as pd

from skfeature.function.similarity_based.fisher_score import fisher_score


def split_feature_by_plus(feature):
    return feature.split('+')


def add_feature():
    raise NotImplementedError


def is_combined_feature(feature, symbol='+'):
    return symbol in feature


def get_feature_part(combined_feature, symbol='+'):
    return combined_feature.split(symbol)


def get_data_matrices_by_features(data_files, features):
    df = read_df_from_files(data_files)
    return extract_matrices_from_df(df, features)


def read_df_from_files(filenames):
    dfs = []
    for filename in filenames:
        dfs.append(pd.read_csv(filename, index_col=0))
    features_df = pd.concat(dfs, axis=1, join='inner')
    return features_df


def extract_matrices_from_df(df, features_list=None):
    features_df = df
    if features_list is not None:
        combinbed_features = [f for f in features_list if is_combined_feature(f, symbol='+')]
        if len(combinbed_features) > 0:
            for f in combinbed_features:
                parts = get_feature_part(f, symbol='+')
                df[f] = df[parts[0]]
                for part in parts[1:]:
                    df[f] = abs(df[f] + df[part])
        features_df = df[features_list]
    features = features_df.columns.values
    codes = list(features_df.index)
    X = features_df.values
    c_count = len([k for k in codes if k.startswith("KT_")])
    pd_count = len([k for k in codes if k.startswith("SG")])
    y = np.zeros((c_count, 1))
    y = np.append(y, np.ones((pd_count, 1)))
    return X, y, features, codes


def shorten_feature_names(features, shorten_sides=True):
    shortened = []
    for feature in features:
        short = feature.replace('JointType.', '') \
            .replace('-eucl_distance', ' E') \
            .replace('-velocity', ' Vm') \
            .replace('-trajectory', ' Tm') \
            .replace('-eucl_div_acceleration', ' E/Am') \
            .replace('-eucl_div_trajectory', ' E/Tm') \
            .replace('-acceleration', ' Am') \
            .replace('-jerk', ' Jm') \
            .replace('-time', ' t')
        if shorten_sides:
            short = short.replace('Right', ' R') \
                .replace('Left', ' L')
        shortened.append(short)
    return shortened


def feature_name_for_tables(feature):
    return feature
    # shortened = shorten_feature_names([feature], shorten_sides=False)[0]
    # shortened = shortened.split()
    # if len(shortened) > 1:
    #     return " & ".join(shortened)
    # else:
    #     return shortened[0] + " & "


def fisher(data_files, save_to_file=None):
    df = read_df_from_files(data_files)
    X, y, features_list, keys = extract_matrices_from_df(df)
    fisher_scores = dict(zip(features_list, fisher_score(X, y)))
    fisher_scores = dict((feature_name_for_tables(k), v) for k, v in fisher_scores.items())  # if
    # 'eucl_div_leg_length' not in k and '_' not in feature_name_for_tables(k))
    result = {"Fisher Score": fisher_scores}
    df = pd.DataFrame(result).sort_values(by='Fisher Score', ascending=0)
    df = df.round(6)
    if save_to_file is not None:
        df.to_csv(save_to_file)
        # pd.DataFrame.to_latex(df, save_to_file, column_format='|l|c|c|')


if __name__ == '__main__':
    # fisher([sys.argv[1]], sys.argv[2])
    # files = [
    #     '/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/mm_1.csv',
    #     '/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/sit_down_mm_1.csv',
    #     '/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/stand_up_mm_1.csv',
    #     '/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/t_1.csv',
    #     '/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/turn_around_mm_1.csv',
    #     '/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/turn_back_around_mm_1.csv',
    #     '/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/walk_forward_mm_1.csv']
    files = ['/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/mm_1.csv',
             '/home/anna/Documents/Masters/Masters/results3/kondimised/features/2/mm_2.csv']
    for file in files:
        head, tail = os.path.split(file)
        fisher_filename = os.path.join(head, f'fisher_{tail.split(".")[0]}.tex')
        fisher([file], fisher_filename)
    # convert_dict_to_matrix('/home/anna/Documents/Masters/Masters/results/mm_2.csv', ["Shoulder R Tm"])
    # data = gather_data_to_matrix(sys.argv[1])
    # write_data_to_csv(data, os.path.join(sys.argv[1], 'data_matrix.csv'))
    # read_matrices_from_file([sys.argv[1]])
    # features = read_dicts_from_files([sys.argv[1]])
    # add_feature(features, 'HEIGHT')
    # data = compute_correlation_matrix([sys.argv[1]])
    # write_data_to_csv(data, sys.argv[2])
