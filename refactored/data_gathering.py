import os
import sys
from os import walk

import pandas as pd


def flatten_dict(data_dict):
    result = {}
    for param, joints_dict in data_dict.items():
        for joint_type, value in joints_dict.items():
            param_name = joint_type + '-' + param
            result[param_name] = value
    return result


def create_group_matrix(filenames):
    result_dict = {}
    for filename in filenames:
        code = extract_code_from_dirpath(filename)
        row = flatten_dict(pd.read_csv(filename, index_col=0).to_dict())
        if not row:
            try:
                with open(filename, 'r') as f:
                    row = {'t': float(f.readline().strip())}
            except:
                print('no data')
        result_dict[code] = row
    return pd.DataFrame.from_dict(result_dict).transpose()


def extract_code_from_dirpath(dirpath):
    head, tail = os.path.split(dirpath)
    code = ''
    while head != '/' and head != '':
        head, tail = os.path.split(head)
        if 'SG' in tail or 'KT' in tail:
            code = tail
    return code


def get_all_directories(dirpath):
    head, tail = os.path.split(dirpath)
    directories = [tail]
    while head != '/' and head != '':
        head, tail = os.path.split(head)
        directories.append(tail)
    return directories


def get_data(rootdir, week, parameter='mm', save_to_file=None, check_phases_not_in_dirpath=True):
    c_files = []
    pd_files = []
    for (dirpath, _, filenames) in walk(rootdir):
        all_directories = get_all_directories(dirpath)
        if week in all_directories and (not check_phases_not_in_dirpath or 'phases' not in all_directories):
            if 'KT' in dirpath:
                c_files += [os.path.join(dirpath, f) for f in filenames if '_' + parameter + '.csv' in f]
            elif 'SG' in dirpath:
                pd_files += [os.path.join(dirpath, f) for f in filenames if '_' + parameter + '.csv' in f]
    print(c_files)
    print(pd_files)
    c_matrix = create_group_matrix(c_files)
    pd_matrix = create_group_matrix(pd_files)

    all_data = pd.concat([c_matrix, pd_matrix])
    if save_to_file is not None:
        all_data.to_csv(save_to_file)


if __name__ == '__main__':
    weeks = ['1', '2']
    parameters = [
        'mm',
        't']
        # 'stand_up_mm',
        # 'sit_down_mm',
        # 'turn_around_mm',
        # 'turn_back_around_mm',
        # 'walk_forward_mm']
    dir_path = sys.argv[1]
    for week in weeks:
        for parameter in parameters:
            # week = sys.argv[2]
            # parameter = sys.argv[3]
            save_to_file = os.path.join(dir_path, f'{parameter}_{week}.csv')
            print(parameter)
            get_data(dir_path, week, parameter, save_to_file, check_phases_not_in_dirpath=TrueA)
