import re
from collections import defaultdict

import matplotlib.pyplot as plt

import kinect
from kinect import JointType


def plot_bone(joints, joint1, joint2, color='b'):
    plt.plot([joints[joint1].x, joints[joint2].x], [joints[joint1].y, joints[joint2].y], color, linewidth=2.7)


def draw_body(body):
    joints = body.joints

    plot_bone(joints, JointType.Head, JointType.SpineShoulder)
    plot_bone(joints, JointType.SpineShoulder, JointType.SpineMid)
    plot_bone(joints, JointType.SpineMid, JointType.SpineBase)
    plot_bone(joints, JointType.SpineShoulder, JointType.ShoulderRight)  # , 'r')
    plot_bone(joints, JointType.SpineShoulder, JointType.ShoulderLeft)
    plot_bone(joints, JointType.SpineBase, JointType.HipRight)  # , 'r')
    plot_bone(joints, JointType.SpineBase, JointType.HipLeft)

    plot_bone(joints, JointType.ShoulderRight, JointType.ElbowRight)  # , 'r')
    plot_bone(joints, JointType.ElbowRight, JointType.WristRight)  # , 'r')
    plot_bone(joints, JointType.WristRight, JointType.HandRight)  # , 'r')

    plot_bone(joints, JointType.ShoulderLeft, JointType.ElbowLeft)
    plot_bone(joints, JointType.ElbowLeft, JointType.WristLeft)
    plot_bone(joints, JointType.WristLeft, JointType.HandLeft)

    plot_bone(joints, JointType.HipRight, JointType.KneeRight)  # , 'r')
    plot_bone(joints, JointType.KneeRight, JointType.AnkleRight)  # , 'r')
    plot_bone(joints, JointType.AnkleRight, JointType.FootRight)  # , 'r')

    plot_bone(joints, JointType.HipLeft, JointType.KneeLeft)
    plot_bone(joints, JointType.KneeLeft, JointType.AnkleLeft)
    plot_bone(joints, JointType.AnkleLeft, JointType.FootLeft)

    # plt.show()


def draw_body_points(body, color_joint_map):
    draw_body(body)
    for joint in body.joints.values():
        if joint.joint_type.name in color_joint_map.keys():
            plt.plot([joint.x], [joint.y],
                     color=color_joint_map[joint.joint_type.name],
                     marker='o',
                     markeredgecolor='k',
                     markeredgewidth=0.9)
        else:
            plt.plot([joint.x], [joint.y], 'ko')
    # plt.show()


def get_default_body():
    line = '23181	468	-0.01383853	0.2843074	3.41674	-0.01773918	0.1002197	3.437898	-0.01235429	-0.1461364	3.399746	-0.004143425	-0.1813413	3.353596	-0.1505542	0.001159028	3.39445	-0.2043616	-0.2003082	3.344068	-0.1740355	-0.3322309	3.216438	-0.1639268	-0.3762051	3.17717	0.1385966	0.009194974	3.447524	0.1720225	-0.1928831	3.376886	0.1518331	-0.3238267	3.246034	0.1451034	-0.3674745	3.205692	-0.06625196	-0.2451386	3.31119	-0.1032591	-0.5086474	3.07498	-0.06860697	-0.7848893	3.024346	-0.05629552	-0.8121886	2.966034	0.06445533	-0.24596	3.352378	0.113456	-0.5094927	3.100259	0.07329275	-0.7717682	3.049154	0.0856042	-0.7990674	2.990841	0	0	0	0	0	0'
    line = line.split('\t')
    return kinect.Body.from_raw_data(line, kinect.KinectVersion.FIRST)


def extract_joint_types(features):
    color_map = defaultdict(str)
    if len(features) > 4:
        return color_map
    colors = ['yellow', 'cyan', 'green', 'red', 'orange']
    for i, feature in enumerate(features):
        matches = re.findall('^(?P<grp1>[^-]+)-', feature)
        matches += re.findall('\+(?P<grp2>[^-]+)-', feature)
        for match in matches:
            color_map[match] = colors[i % len(colors)]
    return color_map


def draw_body_with_features(features, body=None, save_to_file=None):
    if body is None:
        body = get_default_body()
    color_joint_map = extract_joint_types(features)
    plt.clf()
    draw_body_points(body, color_joint_map)
    plt.axes().set_aspect('equal', 'datalim')
    plt.axes().get_xaxis().set_visible(False)
    plt.axes().get_yaxis().set_visible(False)
    plt.gca().invert_xaxis()
    if save_to_file is None:
        plt.show()
    else:
        plt.savefig(save_to_file, bbox_inches='tight')
        plt.close()


if __name__ == '__main__':
    draw_body_with_features(["ShoulderRight-eucl_distance+KneeRight-eucl_distance", "ShoulderLeft-eucl_distance+KneeLeft-eucl_distance"])
