import matplotlib

# matplotlib.use('agg')

import matplotlib.pyplot as plt
import numpy as np
import pandas
import pydotplus
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
from sklearn import tree
import re
import os


def plot_data_3d(C, PD, features, save_to_file=None, ordering=None, annotate=False):
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.scatter(C[:, 0], C[:, 1], C[:, 2], marker='o', c='blue', s=20, label='C', depthshade=True)
    ax.scatter(PD[:, 0], PD[:, 1], PD[:, 2], marker='o', c='red', s=20, label='PD', depthshade=True)
    ax.set_xlabel(features[0])
    ax.set_ylabel(features[1])
    ax.set_zlabel(features[2])
    if annotate and ordering is not None:
        c_annotations = [k for k in ordering if k.startswith('KT_')]
        pd_annotations = [k for k in ordering if k.startswith('SG')]
        for i in range(len(c_annotations)):
            ax.text(C[i, 0], C[i, 1], C[i, 2], '{0}'.format(c_annotations[i]), size=5)
        for i in range(len(pd_annotations)):
            ax.text(PD[i, 0], PD[i, 1], PD[i, 2], '{0}'.format(pd_annotations[i]), size=5)
    plt.grid(True)
    if save_to_file is None:
        plt.show()
    else:
        plt.savefig(save_to_file, bbox_inches='tight')
        plt.close(fig)


def plot_data_2d(C, PD, features, save_to_file=None, ordering=None, annotate=False):
    plt.clf()
    plt.plot(C[:, 0], C[:, 1], 'bo')
    plt.plot(PD[:, 0], PD[:, 1], 'ro')
    plt.xlabel(features[0])
    plt.ylabel(features[1])
    if annotate and ordering is not None:
        annotate_data(C, PD, ordering)
    plt.grid(True)
    if save_to_file is None:
        plt.show()
    else:
        plt.savefig(save_to_file, bbox_inches='tight')
        plt.close()


def plot_data_1d(C, PD, features, save_to_file=None, ordering=None, annotate=False):
    pd_y = max(list(C) + list(PD)) - abs(min(list(C) + list(PD)))
    pd_y /= 10
    plt.axhline(y=0, color='k')
    plt.axhline(y=pd_y, color='k')
    plt.scatter(C, np.zeros_like(C), marker='o', c='blue', zorder=10, alpha=0.7)
    plt.scatter(PD, np.ones_like(PD) * pd_y, marker='o', c='red', zorder=10, alpha=0.7)
    plt.axis('equal')
    plt.xlabel(features[0])
    plt.yticks([0, pd_y], ["C", "PD"])
    if annotate and ordering is not None:
        c_annotations = [k for k in ordering if k.startswith('KT_')]
        pd_annotations = [k for k in ordering if k.startswith('SG')]
        for i, txt in enumerate(c_annotations):
            plt.annotate(txt, (C[i], 0))
        for i, txt in enumerate(pd_annotations):
            plt.annotate(txt, (PD[i], pd_y))
    if save_to_file is None:
        plt.show()
    else:
        plt.savefig(save_to_file, bbox_inches='tight')
        plt.close()


def annotate_data(C, PD, ordering):
    c_annotations = [k for k in ordering if k.startswith('KT_')]
    pd_annotations = [k for k in ordering if k.startswith('SG')]
    for i, txt in enumerate(c_annotations):
        if i % 2 == 0:
            plt.annotate(txt, (C[i, 0], C[i, 1]), xytext=(7, -7),
                         textcoords='offset points', ha='left', va='top')
        else:
            plt.annotate(txt, (C[i, 0], C[i, 1]), xytext=(-7, 7),
                         textcoords='offset points', ha='right', va='bottom')
    for i, txt in enumerate(pd_annotations):
        if i % 2 == 0:
            plt.annotate(txt, (PD[i, 0], PD[i, 1]), xytext=(7, -7),
                         textcoords='offset points', ha='left', va='top')
        else:
            plt.annotate(txt, (PD[i, 0], PD[i, 1]), xytext=(-7, 7),
                         textcoords='offset points', ha='right', va='bottom')


def make_meshgrid(x, y, h=.02):
    x_pad = (x.max() - x.min()) / 10
    y_pad = (y.max() - y.min()) / 10
    x_min, x_max = x.min() - x_pad, x.max() + x_pad
    y_min, y_max = y.min() - y_pad, y.max() + y_pad
    xx, yy = np.meshgrid(np.arange(x_min, x_max + h, h),
                         np.arange(y_min, y_max + h, h))
    return xx, yy


def make_meshgrid_3d(x, y, z, h=.02):
    x_pad = (x.max() - x.min()) / 10
    y_pad = (y.max() - y.min()) / 10
    z_pad = (z.max() - z.min()) / 10
    x_min, x_max = x.min() - x_pad, x.max() + x_pad
    y_min, y_max = y.min() - y_pad, y.max() + y_pad
    z_min, z_max = z.min() - z_pad, z.max() + z_pad
    xx, yy, zz = np.meshgrid(np.arange(x_min, x_max + h, h),
                             np.arange(y_min, y_max + h, h),
                             np.arange(z_min, z_max + h, h))
    return xx, yy, zz


def surface_boundary_all(decision_boundary):
    xyz = {'x': decision_boundary[:, 0], 'y': decision_boundary[:, 1], 'z': decision_boundary[:, 2]}

    df = pandas.DataFrame(xyz, index=range(len(xyz['x'])))

    x = np.linspace(df['x'].min(), df['x'].max(), len(df['x'].unique()))
    y = np.linspace(df['y'].min(), df['y'].max(), len(df['y'].unique()))

    x, y = np.meshgrid(x, y)
    z = griddata((df['x'], df['y']), df['z'], (x, y), method='cubic')

    return x, y, z


def plot_decision_boundary_3d(X, y, clf, features, file_to_save_to=None):
    labels = ["C", "PD"]
    color_map = "bwr"
    plot_colors = "br"
    X0, X1, X2 = X[:, 0], X[:, 1], X[:, 2]
    xx, yy, zz = make_meshgrid_3d(X0, X1, X2)
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel(), zz.ravel()])
    Z = Z.reshape(xx.shape)
    fig = plt.figure()
    ax = Axes3D(fig)
    for i, color in zip(range(2), plot_colors):
        idx = np.where(y == i)
        ax.scatter(X[idx, 0], X[idx, 1], X[idx, 1], c=color, label=labels[i],
                   cmap=plt.get_cmap(color_map), edgecolor='black', s=15)
    # ax.contourf(xx, yy, zz, Z, cmap=plt.get_cmap(color_map), alpha=0.3)

    I, J, K = xx.shape
    decision_boundary = np.empty((0, 3))
    boundary_label = np.array([])
    for i in range(1, I - 1):
        for j in range(1, J - 1):
            for k in range(1, K - 1):
                cube_neighborhood = Z[i - 1:i + 2, j - 1:j + 2, k - 1:k + 2]
                cube_etalon = np.full((3, 3, 3), Z[i, j, k])

                if not np.array_equal(cube_etalon, cube_neighborhood):
                    decision_boundary = np.append(decision_boundary,
                                                  [np.array([xx[i, j, k], yy[i, j, k], zz[i, j, k]])],
                                                  axis=0)
                    boundary_label = np.append(boundary_label, Z[i, j, k])
    if decision_boundary.size != 0:
        try:
            x, y, z = surface_boundary_all(decision_boundary)
            ax.plot_surface(x, y, z, rstride=1, cstride=1, color='k',
                            linewidth=0, antialiased=True, alpha=0.3)
        except Exception:
            pass
    ax.set_xlabel(features[0])
    ax.set_ylabel(features[1])
    ax.set_zlabel(features[2])
    # ax.axis("tight")
    plt.grid(True)
    if file_to_save_to is None:
        plt.show()
    else:
        plt.savefig(file_to_save_to, bbox_inches='tight')
        plt.close()


def plot_decision_boundary_2d(X, y, clf, features, file_to_save_to=None):
    labels = ["C", "PD"]
    color_map = "Pastel1_r"
    plot_colors = "br"
    X0, X1 = X[:, 0], X[:, 1]
    xx, yy = make_meshgrid(X0, X1)
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    cs = plt.contourf(xx, yy, Z, cmap=plt.get_cmap(color_map))
    for i, color in zip(range(2), plot_colors):
        idx = np.where(y == i)
        plt.scatter(X[idx, 0], X[idx, 1], c=color, label=labels[i],
                    cmap=plt.get_cmap(color_map), edgecolor='black', s=15)
    plt.xlabel(features[0])
    plt.ylabel(features[1])
    plt.axis("tight")
    plt.grid(True)
    if file_to_save_to is None:
        plt.show()
    else:
        plt.savefig(file_to_save_to, bbox_inches='tight')
        plt.close()


def generate_tree_visualization_pdf(clf, features, path, filename_part, rounded=False, filled=False):
    tree.export_graphviz(clf,
                         feature_names=features,
                         out_file=os.path.join(path, 'tree.dot'),
                         rounded=rounded,
                         filled=filled,
                         impurity=False,
                         class_names=True)
    f = pydotplus.graph_from_dot_file(os.path.join(path, "tree.dot")).to_string()
    f = re.sub('samples = [0-9]+', '', f)
    f = re.sub('value = \[[0-9]+, [0-9]+\]', '', f)
    f = re.sub('\\\\n\\\\n', '', f)
    with open(os.path.join(path, 'tree_modified.dot'), 'w') as file:
        file.write(f)
    graph = pydotplus.graph_from_dot_file(os.path.join(path, 'tree_modified.dot'))
    graph.write_pdf(os.path.join(path, filename_part + ".pdf"))
    graph.write_png(os.path.join(path, filename_part + ".png"))


def extract_classes(X, y):
    c_indices = np.where(y == 0)[0]
    pd_indices = np.where(y == 1)[0]
    if X.size / X.shape[0] == 1:
        C = X[c_indices]
        PD = X[pd_indices]
    else:
        C = X[c_indices, :]
        PD = X[pd_indices, :]
    return C, PD


def plot_data(X, y, features, save_to_file=None, ordering=None, annotate=False):
    C, PD = extract_classes(X, y)
    if len(features) == 1:
        plot_data_1d(C, PD, features, save_to_file, ordering=ordering, annotate=annotate)
        return True
    elif len(features) == 2:
        plot_data_2d(C, PD, features, save_to_file, ordering=ordering, annotate=annotate)
        return True
    elif len(features) == 3:
        plot_data_3d(C, PD, features, save_to_file, ordering=ordering, annotate=annotate)
        return True
    else:
        return False
