import os
import sys
from collections import defaultdict

import numpy as np
import pandas
import pdfkit
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
from sklearn.model_selection import KFold, train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from data_processing import apply_pca, shorten_feature_names
# matplotlib.use('agg')
from draw import draw_body_with_features
from draw_data import plot_decision_boundary_3d, plot_decision_boundary_2d, plot_data, generate_tree_visualization_pdf
from refactored.data_processing import get_data_matrices_by_features


def train_model(clf, x_train, y_train):
    if x_train.size / x_train.shape[0] == 1:
        clf.fit(x_train.reshape(-1, 1), y_train)
    else:
        clf.fit(x_train, y_train)


def validate_model(clf, x_test, y_test):
    if x_test.size / x_test.shape[0] == 1:
        x_test = x_test.reshape(-1, 1)
    accuracy = clf.score(x_test, y_test)
    predicted = clf.predict(x_test)
    confusion_matrix = metrics.confusion_matrix(y_test, predicted)
    target_names = ["C", "PD"]
    report = classification_report(y_test, predicted, target_names=target_names)
    report = report.replace("\n\n", "\n")
    report = report.replace("avg / total", "avg/total")
    report = report.replace("micro avg", "micro_avg")
    report = report.replace("macro avg", "macro_avg")
    report = report.replace("weighted avg", "weighted_avg")
    lines = report.split("\n")[:-1]
    report_dict = defaultdict(dict)
    params = lines[0].split()
    for param in params:
        report_dict[param] = defaultdict(float)
    for line in lines[1:]:
        values = line.split()
        for i, param in enumerate(params):
            report_dict[param][values[0]] = float(values[i + 1])
    return accuracy, confusion_matrix, report_dict


# def print_info(X, y, header):
#     C, PD = extract_classes(X, y)
#     for index, param in enumerate(header):
#         print(param)
#         c_param = C[:, index]
#         pd_param = PD[:, index]
#         print("\tC std:\t\t", np.std(c_param))
#         print("\tC mean:\t\t", np.mean(c_param))
#         print("\tPD std:\t\t", np.std(pd_param))
#         print("\tPD mean:\t", np.mean(pd_param))


def data_frame_to_table_html(df):
    return df.to_html().replace('<table border="1" class="dataframe">',
                                '<table class="table table-bordered table-condensed">')


def wrap_picture_name_with_html(picture_name):
    return '<img class="img-responsive" style="margin: 0 auto; zoom:75%" src="img/' + picture_name + '"></img>'


def get_avg_reports(reports):
    all_reports = defaultdict(list)
    for report_dict in reports:
        for clf, report in report_dict.items():
            all_reports[clf].append(report)
    avg_reports = defaultdict()
    for clf, report_list in all_reports.items():
        avg_reports[clf] = report_list[0]
        for report in report_list[1:]:
            avg_reports[clf] = avg_reports[clf] + report
        avg_reports[clf] = avg_reports[clf] / len(report_list)
        avg_reports[clf] = avg_reports[clf].round(2)
    return avg_reports


def add_stats_to_confusion_matrix(df):
    df['Precision'] = (df['tp'] / (df['tp'] + df['fp'])).round(2)
    df['Recall'] = (df['tp'] / (df['tp'] + df['fn'])).round(2)
    df['F1'] = ((2 * df['Precision'] * df['Recall']) / (df['Precision'] + df['Recall'])).round(2)


def generate_models(classifiers, features, X, y, save_to_directory=None, plot_db=False):
    two_dimensional_data = (len(features) == 2)
    three_dimensional_data = (len(features) == 3)
    html = ''
    confusion_matrices = {'tn': defaultdict(list),
                          'fp': defaultdict(list),
                          'fn': defaultdict(list),
                          'tp': defaultdict(list)}
    confusion_matrix_separate = {'tn': defaultdict(float),
                                 'fp': defaultdict(float),
                                 'fn': defaultdict(float),
                                 'tp': defaultdict(float)}
    confusion_matrices_separate = []
    accuracies = defaultdict(list)
    k = 4
    kf = KFold(n_splits=k, shuffle=True, random_state=42)
    # kf = Bootstrap(len(y), n_bootstraps=3, n_train=0.5, n_test=None, random_state=None)
    i = -1
    decision_boundary_pics = []
    reports = []
    for train_index, test_index in kf.split(X):
        reports.append({})
        i += 1
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        if save_to_directory is not None:
            plot_data(X_train, y_train, features, os.path.join(save_to_directory, str(i) + '_fold.png'))
        for clf in classifiers:
            train_model(clf, X_train, y_train)
            if three_dimensional_data and plot_db:
                pic_name = str(i) + clf.__class__.__name__ + '_db.png'
                decision_boundary_pics.append(pic_name)
                plot_decision_boundary_3d(X, y, clf, features,
                                          os.path.join(save_to_directory, pic_name))
            elif two_dimensional_data and plot_db:
                pic_name = str(i) + clf.__class__.__name__ + '_db.png'
                decision_boundary_pics.append(pic_name)
                plot_decision_boundary_2d(X, y, clf, features,
                                          os.path.join(save_to_directory, pic_name))
            accuracy, confusion_matrix, report = validate_model(clf, X_test, y_test)
            report = pandas.DataFrame.from_dict(report)
            reports[i].update({clf.__class__.__name__: report})
            accuracies[clf.__class__.__name__].append(np.round(accuracy * 100, 2))
            tn, fp, fn, tp = confusion_matrix.ravel()
            confusion_matrices['tn'][clf.__class__.__name__].append(tn)
            confusion_matrices['fp'][clf.__class__.__name__].append(fp)
            confusion_matrices['fn'][clf.__class__.__name__].append(fn)
            confusion_matrices['tp'][clf.__class__.__name__].append(tp)

            confusion_matrix_separate['tn'][clf.__class__.__name__] = tn
            confusion_matrix_separate['fp'][clf.__class__.__name__] = fp
            confusion_matrix_separate['fn'][clf.__class__.__name__] = fn
            confusion_matrix_separate['tp'][clf.__class__.__name__] = tp
            if isinstance(clf, DecisionTreeClassifier) and save_to_directory is not None:
                filename_part = clf.__class__.__name__ + str(i)
                generate_tree_visualization_pdf(clf, features, save_to_directory, filename_part)
                generate_tree_visualization_pdf(clf, features, save_to_directory, filename_part + "_colored",
                                                rounded=True,
                                                filled=True)
        confusion_matrices_separate.append(confusion_matrix_separate)
    avg_accuracies = {'Average Accuracy': {k: np.round(np.mean(v), 2) for k, v in accuracies.items()}}
    df = pandas.DataFrame(avg_accuracies)
    html += '<h3>Average Accuracy</h3>' + data_frame_to_table_html(df)

    avg_matrices = confusion_matrices
    for key, value in confusion_matrices.items():
        for k, v in value.items():
            avg_matrices[key][k] = np.round(np.mean(v), 2)
    df = pandas.DataFrame(avg_matrices)
    add_stats_to_confusion_matrix(df)
    html += '<h3>Average Confusion Matrices</h3>' + data_frame_to_table_html(df)

    avg_reports = get_avg_reports(reports)
    for clf, report in avg_reports.items():
        html += '<h3> Average Report for ' + clf + '</h3>' + data_frame_to_table_html(report)

    df = pandas.DataFrame(accuracies).transpose()
    html += '<h3>Accuracy for all of the folds</h3>' + data_frame_to_table_html(df)

    for i, matrix in enumerate(confusion_matrices_separate):
        df = pandas.DataFrame(matrix)
        add_stats_to_confusion_matrix(df)
        html += '<h3>' + str(i) + ' fold </h3>' + data_frame_to_table_html(df)
        html += wrap_picture_name_with_html(str(i) + '_fold.png')
        html += wrap_picture_name_with_html('DecisionTreeClassifier' + str(i) + '_colored.png')
        html += '<div class="row imagetiles">'
        for pic_name in decision_boundary_pics:
            if pic_name.startswith(str(i)):
                html += '<div style="text-align: center" class="col-lg-3 col-md-3 col-sm-3 col-xs-6">' + \
                        pic_name.split('.')[0].split('_')[0][1:] + \
                        wrap_picture_name_with_html(pic_name) + '</div>'
        html += '</div>'
        for k, v in reports[i].items():
            html += '<h4> Report for ' + k + '</h4>' + data_frame_to_table_html(v)
    return html


def combine_features(X, header, features):
    new_X = None
    for feature in features:
        feature_vector = np.zeros_like(X[:, 0])
        if '+' in feature:
            parameters = feature.split('+')
            for parameter in parameters:
                feature_vector = np.add(feature_vector, X[:, header.index(parameter)])
        else:
            feature_vector = X[:, header.index(feature)]
        if new_X is None:
            new_X = feature_vector
        else:
            new_X = np.column_stack((new_X, feature_vector))
    return new_X


def create_report(data_files, directory, features):
    if not os.path.exists(directory):
        os.makedirs(directory, exist_ok=True)
        img_path = os.path.join(directory, 'img')
        os.makedirs(img_path)
    else:
        print("Directory", directory, "already exists! Please choose another name.")
        return
    html = '<html lang="en"><head>' \
           '<meta charset="utf-8">' \
           '<meta name="viewport" content="width=device-width, initial-scale=1">' \
           '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">' \
           '<style>div.imagetiles div.col-lg-3.col-md-3.col-sm-3.col-xs-6{padding: 0px;}</style>' \
           '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>' \
           '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>' \
           '</head><body><div class="container">'
    X, y, parts, ordering = get_data_matrices_by_features(data_files, features)
    draw_body_with_features(features, save_to_file=os.path.join(img_path, 'features.png'))
    # initial_features = deepcopy(features)
    features = shorten_feature_names(features)
    html += '<h3>Features:</h3>' + '<br>'.join(features) + '<br>'
    html += '<br>' + ', '.join(ordering) + '<br>'
    html += wrap_picture_name_with_html('features.png')
    plotted = plot_data(X, y, features, save_to_file=os.path.join(img_path, 'data.png'), ordering=ordering,
                        annotate=False)
    if plotted:
        html += wrap_picture_name_with_html('data.png')
    classifiers = [LogisticRegression(),
                   KNeighborsClassifier(),
                   SVC(),
                   DecisionTreeClassifier()]
    html += generate_models(classifiers, features, X, y, save_to_directory=img_path, plot_db=False)
    html += '</div></body></html>'
    with open(os.path.join(directory, 'report.html'), 'w') as f:
        f.write(html)
    pdfkit.from_file(os.path.join(directory, 'report.html'), os.path.join(directory, 'report.pdf'))


def show_data(data_file, features, annotate=False, save_to_file=None):
    X, y, parts, ordering = get_data_matrices_by_features([data_file], features)
    X = combine_features(X, parts, features)
    features = shorten_feature_names(features)
    plot_data(X, y, features, ordering=ordering, annotate=annotate, save_to_file=save_to_file)


def show_pca_data(data_files, n_components):
    X, y = apply_pca(data_files, n_components)
    features = []
    for i in range(n_components):
        features.append(str(i))
    plot_data(X, y, features)


def train_model_to_get_data(clf, data_files, directory, features):
    if not os.path.exists(directory):
        os.makedirs(directory, exist_ok=True)
    else:
        print("Directory", directory, "already exists! Please choose another name.")
    X, y, parts, ordering = get_data_matrices_by_features(data_files, features)
    # draw_body_with_features(features, save_to_file=os.path.join(img_path, 'features.png'))
    # initial_features = deepcopy(features)
    features = shorten_feature_names(features)
    # plotted = plot_data(X, y, features, save_to_file=os.path.join(img_path, 'data.png'), ordering=ordering,
    #                     annotate=False)
    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.7, random_state=42)
    train_model(clf, X_train, y_train)
    accuracy, confusion_matrix, report = validate_model(clf, X_test, y_test)
    print(accuracy)
    print(confusion_matrix)
    print(report)
    pic_name = clf.__class__.__name__ + '_db.png'
    plot_decision_boundary_2d(X, y, clf, features,
                              os.path.join(directory, pic_name))


def train_test_different_weeks(features):
    clf = KNeighborsClassifier()
    X_train, y_train, parts, ordering = get_data_matrices_by_features(['/home/anna/Documents/Masters/Masters/results3/kondimised/features/2/mm_2.csv'], features)
    X_test, y_test, parts, ordering = get_data_matrices_by_features(['/home/anna/Documents/Masters/Masters/results3/kondimised/features/1/mm_1.csv'], features)
    features = shorten_feature_names(features)
    train_model(clf, X_train, y_train)
    # accuracy, confusion_matrix, report = validate_model(clf, X_test, y_test)
    if X_test.size / X_test.shape[0] == 1:
        x_test = X_test.reshape(-1, 1)
    accuracy = clf.score(X_test, y_test)
    predicted = clf.predict(X_test)
    confusion_matrix = metrics.confusion_matrix(y_test, predicted)
    target_names = ["C", "PD"]
    report = classification_report(y_test, predicted, target_names=target_names)
    print(accuracy)
    print(confusion_matrix)
    print(report)
    pic_name = clf.__class__.__name__ + '_db.png'
    plot_decision_boundary_2d(X_test, y_test, clf, features,
                              os.path.join(directory, pic_name))


if __name__ == '__main__':
    data_file, directory, features = sys.argv[1], sys.argv[2], sys.argv[3:]
    # show_data(data_file, features, annotate=False)
    # , save_to_file="/home/anna/Documents/Masters/article/journal_article/best_model.pdf")
    # show_pca_data([data_file], 3)
    create_report([data_file], directory, features)
    # train_model_to_get_data(KNeighborsClassifier(), [data_file], directory, features)
    # train_test_different_weeks(features)
