import os
import sys
from collections import defaultdict
from os import walk

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA

from data.read_data import get_data_from_table
from skfeature.function.similarity_based.fisher_score import fisher_score

averages_mm_filename = "nadal_2_avg_Kondimised_mm_steps_week.csv"
averages_angles_filename = "nadal_2_avg_Kondimised_angles_steps_week.csv"
# averages_mm_filename = "nadal_2_avg_Alustamised_mm_steps_week.csv"
# averages_angles_filename = "nadal_2_avg_Alustamised_angles_steps_week.csv"
averages_time_filename = "avg_experiment_time.csv"
averages_time_filename2 = "avg_experiment_time2.csv"
averages_time_filename3 = "avg_experiment_time3.csv"


# averages_time_filename = "avg_forward_walking_time.csv"


def _get_code_from_dirpath(dirpath):
    head = dirpath
    tail = ''
    while not tail.startswith('KT_') and not tail.startswith('SG'):
        head, tail = os.path.split(head)
    return tail


def _flatten_dict(features):
    flattened_features = defaultdict(float)
    for param, joint_dict in features.items():
        for joint_type, value in joint_dict.items():
            flattened_features[joint_type + '-' + param] = value
    return flattened_features


def _read_value_from_file(filename):
    with open(filename) as data:
        lines = data.readlines()
        value = float(lines[0])
    return value


def gather_data_to_matrix(dir):
    data = defaultdict(dict)
    for (dirpath, _, filenames) in walk(dir):
        if averages_mm_filename in filenames:
            features_filepath = os.path.join(dirpath, averages_mm_filename)
            print(os.path.join(dirpath, averages_mm_filename))
            features = pd.read_csv(features_filepath, index_col=0).to_dict()
            data[_get_code_from_dirpath(dirpath)].update(_flatten_dict(features))
        if averages_angles_filename in filenames:
            features_filepath = os.path.join(dirpath, averages_angles_filename)
            print(os.path.join(dirpath, averages_angles_filename))
            features = pd.read_csv(features_filepath, index_col=0).to_dict()
            data[_get_code_from_dirpath(dirpath)].update(_flatten_dict(features))
        if averages_time_filename in filenames and 'nadal_2' in dirpath:
            time = _read_value_from_file(os.path.join(dirpath, averages_time_filename))
            data[_get_code_from_dirpath(dirpath)].update({'time': time})
        if averages_time_filename2 in filenames and 'nadal_2' in dirpath:
            time = _read_value_from_file(os.path.join(dirpath, averages_time_filename2))
            data[_get_code_from_dirpath(dirpath)].update({'time2': time})
        if averages_time_filename3 in filenames and 'nadal_2' in dirpath:
            time = _read_value_from_file(os.path.join(dirpath, averages_time_filename3))
            data[_get_code_from_dirpath(dirpath)].update({'time3': time})
    return data


def write_data_to_csv(data, filepath):
    if os.path.exists(filepath):
        print("File already exists!")
        return
    pd.DataFrame(data).transpose().to_csv(filepath)


def read_dicts_from_files(filepaths):
    features_combined = defaultdict(dict)
    keys = []
    feature_dicts = []
    for filepath in filepaths:
        features = pd.read_csv(filepath, index_col=0).transpose().to_dict()
        feature_dicts.append(features)
        keys = keys + list(features.keys())
    keys = sorted(list(set(keys)))
    for key in keys:
        features_combined[key] = defaultdict(dict)
        for feature_dict in feature_dicts:
            features_combined[key].update(feature_dict[key])
    return features_combined


def add_feature(feature_dict, feature_to_add_key):
    result = defaultdict(dict)
    data, pd_data = get_data_from_table()
    for key, value in feature_dict.items():
        if key in data.keys() and feature_to_add_key in data[key].keys():
            result[key] = value
            result[key].update({feature_to_add_key: data[key][feature_to_add_key]})
    return result


def convert_dict_to_matrix(features, features_list=None):
    keys = sorted(list(features.keys()))
    if features_list is None:
        features_list = sorted(list(features[keys[0]].keys()))
    X = np.array([])
    for key in keys:
        for f in features_list:
            X = np.append(X, features[key][f])
    X = X.reshape((len(keys), len(features_list)))
    c_count = len([k for k in features.keys() if k.startswith("KT_")])
    pd_count = len([k for k in features.keys() if k.startswith("SG")])
    y = np.zeros((c_count, 1))
    y = np.append(y, np.ones((pd_count, 1)))
    return X, y, features_list, keys


def get_labels(keys):
    c_count = len([k for k in keys if k.startswith("KT_")])
    pd_count = len([k for k in keys if k.startswith("SG")])
    y = np.zeros((c_count, 1))
    y = np.append(y, np.ones((pd_count, 1)))


def get_only_needed_features(features_dict, features):
    result = defaultdict(dict)
    for key, value in features_dict.items():
        result[key] = defaultdict(dict)
        for k, v in value.items():
            if k in features:
                result[key][k] = v
    return result


def split_feature_by_plus(feature):
    return feature.split('+')


def get_data_matrices_by_features(data_files, features):
    features_dict = read_dicts_from_files(data_files)
    feature_names = features_dict[list(features_dict.keys())[0]].keys()
    all_parts = []
    for feature in features:
        parts = split_feature_by_plus(feature)
        all_parts = all_parts + parts
        for part in parts:
            if part not in feature_names:
                features_dict = add_feature(features_dict, part)
    features_dict = get_only_needed_features(features_dict, all_parts)
    return convert_dict_to_matrix(features_dict, all_parts)


def compute_correlation_matrix(data_files):
    features_dict = read_dicts_from_files(data_files)
    df = pd.DataFrame(features_dict).transpose()
    corr_matrix = df.corr().abs()
    return corr_matrix


def apply_pca(data_files, n_components):
    features_dict = read_dicts_from_files(data_files)
    X, y, features_list, keys = convert_dict_to_matrix(features_dict)
    pca = PCA(n_components=n_components)
    X_new = pca.fit_transform(X)
    return X_new, y


def shorten_feature_names(features, shorten_sides=True):
    shortened = []
    for feature in features:
        short = feature.replace('JointType.', '') \
            .replace('-eucl_distance', ' E') \
            .replace('-velocity', ' Vm') \
            .replace('-trajectory', ' Tm') \
            .replace('-eucl_div_acceleration', ' E/Am') \
            .replace('-eucl_div_trajectory', ' E/Tm') \
            .replace('-acceleration', ' Am') \
            .replace('-jerk', ' Jm') \
            .replace('-time', ' t')
        if shorten_sides:
            short = short.replace('Right', ' R')\
                .replace('Left', ' L')
        shortened.append(short)
    return shortened


def feature_name_for_tables(feature):
    shortened = shorten_feature_names([feature], shorten_sides=False)[0]
    shortened = shortened.split()
    if len(shortened) > 1:
        return " & ".join(shortened)
    else:
        return shortened[0] + " & "


def fisher(data_files, save_to_file=None):
    features_dict = read_dicts_from_files(data_files)
    X, y, features_list, keys = convert_dict_to_matrix(features_dict)
    fisher_scores = dict(zip(features_list, fisher_score(X, y)))
    fisher_scores = dict((feature_name_for_tables(k), v) for k, v in fisher_scores.items() if
                         'eucl_div_leg_length' not in k and '_' not in feature_name_for_tables(k))
    result = {"Fisher Score": fisher_scores}
    df = pd.DataFrame(result).sort_values(by='Fisher Score', ascending=0)
    df = df.round(6)
    if save_to_file is not None:
        pd.DataFrame.to_latex(df, save_to_file, column_format='|l|c|')


if __name__ == '__main__':
    fisher([sys.argv[1]], "/home/anna/Documents/Masters/all_params/fisher.tex")
    # data = gather_data_to_matrix(sys.argv[1])
    # write_data_to_csv(data, os.path.join(sys.argv[1], 'data_matrix.csv'))
    # read_matrices_from_file([sys.argv[1]])
    # features = read_dicts_from_files([sys.argv[1]])
    # add_feature(features, 'HEIGHT')
    # data = compute_correlation_matrix([sys.argv[1]])
    # write_data_to_csv(data, sys.argv[2])
