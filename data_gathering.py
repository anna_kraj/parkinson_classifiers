import os
from numpy import array, average
from os import walk
import re

averages_mm_filename = "nadal_2_avg_Kondimised_mm_steps_week.csv"
averages_mm_filename2 = "nadal_1_avg_Alustamised_mm_steps_week.csv"
averages_angles_filename = "nadal_2_avg_Kondimised_angles_steps_week.csv"
averages_mm_part = "_avg_Kondimised_mm_steps_week.csv"
averages_angles_part = "_avg_Kondimised_angles_steps_week.csv"
# general_averages = "average angles.csv"
# random_type = "JointType.Head"
random_type = "left_hip"
delimiter = ','
ordering = []


def write_data_to_file(result, save_to_file):
    operation = "a" if os.path.exists(save_to_file) else "w"
    with open(save_to_file, operation) as csvfile:
        csvfile.write(delimiter.join([str(num) for num in result]) + " \n")


def save_ordering(filename):
    global ordering
    head = filename
    tail = ""
    for i in range(3):
        head, tail = os.path.split(head)
    if len(tail) > 0 and tail not in ordering:
        ordering.append(tail)


def read_features_from_file(filename):
    save_ordering(filename)
    features = []
    with open(filename) as data:
        lines = data.readlines()
        for i in range(1, len(lines)):
            line = lines[i].split(delimiter)
            for j in range(1, len(line)):
                features.append(float(line[j]))
    return features


def make_header(filepath):
    param_labels = []
    with open(filepath) as data:
        lines = data.readlines()
        params = lines[0].split(delimiter)[1:]
        joint_types = []
        for i in range(1, len(lines)):
            joint_types.append(lines[i].split(delimiter)[0])
        for joint_type in joint_types:
            for param in params:
                param = param.replace("\n", "")
                param_labels.append(joint_type + "-" + param)
    return param_labels


y = []
all_features = []
sample_size = 0
features_count = 0
header = []


def read_avg_time(dirpath):
    dirpath = re.sub("27th_march_3", "time3", dirpath)
    filepath = os.path.join(dirpath, "avg_experiment_time.csv")
    if os.path.isfile(filepath):
        print("time", filepath)
        with open(filepath) as data:
            lines = data.readlines()
            value = float(lines[0])
        return value
    return None


def collect_data_extended(dir, label):
    global all_features
    global y
    global sample_size
    global features_count
    global header
    for (dirpath, _, filenames) in walk(dir):
        # if averages_mm_filename in filenames:
        #     sample_size += 1
        #     mm_filepath = os.path.join(dirpath, averages_mm_filename)
        #     if len(header) == 0:
        #         header = make_header(mm_filepath)
        #     print(os.path.join(dirpath, averages_mm_filename))
        #     features = read_features_from_file(mm_filepath)
        #     if features_count == 0:
        #         features_count = len(features)
        #     all_features = all_features + features
        #     y.append(label)
        if averages_mm_filename2 in filenames:
            sample_size += 1
            mm_filepath = os.path.join(dirpath, averages_mm_filename2)
            if len(header) == 0:
                header = make_header(mm_filepath)
            print(os.path.join(dirpath, averages_mm_filename2))
            features = read_features_from_file(mm_filepath)
            if features_count == 0:
                features_count = len(features)
            all_features = all_features + features
            y.append(label)


def collect_data(dir, label):
    global all_features
    global y
    global sample_size
    global features_count
    global header
    for (dirpath, _, filenames) in walk(dir):
        if averages_mm_filename in filenames and averages_angles_filename in filenames:
            # avg_time = read_avg_time(dirpath)
            # if avg_time is None:
            #     continue
            sample_size += 1
            mm_filepath = os.path.join(dirpath, averages_mm_filename)
            angles_filepath = os.path.join(dirpath, averages_angles_filename)
            if len(header) == 0:
                header = make_header(mm_filepath) + make_header(angles_filepath)# + ["avg_experiment_time"]
            print(os.path.join(dirpath, averages_mm_filename))
            features = read_features_from_file(mm_filepath) + read_features_from_file(angles_filepath)# + [avg_time]
            if features_count == 0:
                features_count = len(features)
            all_features = all_features + features
            y.append(label)


def collect_data_diff_weeks(dir, nadal, label):
    global all_features
    global y
    global sample_size
    global features_count
    global header
    for (dirpath, _, filenames) in walk(dir):
        if nadal + averages_mm_part in filenames and nadal + averages_angles_part in filenames:
            sample_size += 1
            mm_filepath = os.path.join(dirpath, nadal + averages_mm_part)
            angles_filepath = os.path.join(dirpath, nadal + averages_angles_part)
            if len(header) == 0:
                header = make_header(mm_filepath) + make_header(angles_filepath)
            print(os.path.join(dirpath, nadal + averages_mm_part))
            features = read_features_from_file(mm_filepath) + read_features_from_file(angles_filepath)
            if features_count == 0:
                features_count = len(features)
            all_features = all_features + features
            y.append(label)


def extract_features_from_files(dirpath, mm_filename, angles_filename, label):
    global sample_size, header, features_count, all_features, y
    sample_size += 1
    mm_filepath = os.path.join(dirpath, mm_filename)
    angles_filepath = os.path.join(dirpath, angles_filename)
    if len(header) == 0:
        header = make_header(mm_filepath) + make_header(angles_filepath)
    print(os.path.join(dirpath, mm_filename))
    features = read_features_from_file(mm_filepath) + read_features_from_file(angles_filepath)
    if features_count == 0:
        features_count = len(features)
    all_features = all_features + features
    y.append(label)


def extract_avg_features_from_files(dirpath, mm_filename, angles_filename, mm_filename2, angles_filename2, label):
    global sample_size, header, features_count, all_features, y
    sample_size += 1
    mm_filepath = os.path.join(dirpath, mm_filename)
    angles_filepath = os.path.join(dirpath, angles_filename)
    mm_filepath2 = os.path.join(dirpath, mm_filename2)
    angles_filepath2 = os.path.join(dirpath, angles_filename2)
    if len(header) == 0:
        header = make_header(mm_filepath) + make_header(angles_filepath)
    print(os.path.join(dirpath, mm_filename))
    features = read_features_from_file(mm_filepath) + read_features_from_file(angles_filepath)
    features2 = read_features_from_file(mm_filepath2) + read_features_from_file(angles_filepath2)
    general_features = []
    for i in range(len(features)):
        general_features.append(average([features[i], features2[i]]))
    if features_count == 0:
        features_count = len(general_features)
    all_features = all_features + general_features
    y.append(label)


def collect_data_by_recordings(dir, nadal, label):
    for (dirpath, _, filenames) in walk(dir):
        mm_filename_i_kord = ""
        angles_filename_i_kord = ""
        mm_filename_ii_kord = ""
        angles_filename_ii_kord = ""
        for filename in filenames:
            if filename.find(nadal) != -1 and filename.find(" I kord") == -1 and filename.find("_mm_avg.csv") != -1:
                mm_filename_i_kord = filename
            elif filename.find(nadal) != -1 and filename.find(" I kord") == -1 and filename.find(
                    "_angles_avg.csv") != -1:
                angles_filename_i_kord = filename
            elif filename.find(nadal) != -1 and filename.find(" II kord") == -1 and filename.find("_mm_avg.csv") != -1:
                mm_filename_ii_kord = filename
            elif filename.find(nadal) != -1 and filename.find(" II kord") == -1 and filename.find(
                    "_angles_avg.csv") != -1:
                angles_filename_ii_kord = filename
        if mm_filename_i_kord != "" and angles_filename_i_kord != "":
            extract_features_from_files(dirpath, mm_filename_i_kord, angles_filename_i_kord, label)
        if mm_filename_ii_kord != "" and angles_filename_ii_kord != "":
            extract_features_from_files(dirpath, mm_filename_ii_kord, angles_filename_ii_kord, label)
        # if mm_filename_i_kord != "" and angles_filename_i_kord != "" and mm_filename_ii_kord != "" and angles_filename_ii_kord != "":
        #     extract_avg_features_from_files(dirpath, mm_filename_i_kord, angles_filename_i_kord, mm_filename_ii_kord,
        #                                     angles_filename_ii_kord, label)


# c_rootdir = sys.argv[1]
# pd_rootdir = sys.argv[2]
# save_to_file = sys.argv[3]
# f = []

def get_data():
    # c_rootdir = "/home/anna/Documents/Masters/more_params/C"
    # pd_rootdir = "/home/anna/Documents/Masters/more_params/PD"

    # c_rootdir = "/home/anna/Documents/Masters/parameters_by_step/C"
    # pd_rootdir = "/home/anna/Documents/Masters/parameters_by_step/PD"

    # c_rootdir = "/home/anna/Documents/Masters/fixed/C"
    # pd_rootdir = "/home/anna/Documents/Masters/fixed/PD"

    # c_rootdir = "/home/anna/Documents/Masters/with_sum/C"
    # pd_rootdir = "/home/anna/Documents/Masters/with_sum/PD"

    # c_rootdir = "/home/anna/Documents/Masters/27th_march_3/C"
    # pd_rootdir = "/home/anna/Documents/Masters/27th_march_3/PD"

    # c_rootdir = "/home/anna/Documents/Masters/with_go_test/C"
    # pd_rootdir = "/home/anna/Documents/Masters/with_go_test/PD"

    # c_rootdir = "/home/anna/Documents/Masters/alustamised/C"
    # pd_rootdir = "/home/anna/Documents/Masters/alustamised/PD"

    c_rootdir = "/home/anna/Documents/Masters/relative_coordinates/C"
    pd_rootdir = "/home/anna/Documents/Masters/relative_coordinates/PD"

    # collect_data_extended(c_rootdir, 0)
    # collect_data_extended(pd_rootdir, 1)

    collect_data(c_rootdir, 0)
    collect_data(pd_rootdir, 1)

    # collect_data_diff_weeks(c_rootdir, "nadal_1", 0)
    # collect_data_diff_weeks(c_rootdir, "nadal_3", 1)

    # collect_data_by_recordings(c_rootdir, " II nadal", 0)
    # collect_data_by_recordings(pd_rootdir, " II nadal", 1)

    print(sample_size, features_count)

    features_array = array(all_features).reshape(sample_size, features_count)
    labels_array = array(y).reshape(sample_size, )
    print(ordering)
    return sample_size, features_count, features_array, labels_array, header


if __name__ == '__main__':
    get_data()
